<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ReportFilter  form
 */
class ReportFilterForm extends Model
{

    const VIEW_HTML = "html";
    const VIEW_PDF = "pdf";

    const VIEW_TYPE_DETAILED = 'detailed';
    const VIEW_TYPE_SHORT = 'short';

    const SPLIT_LEVEL_TRANSACTION = 'transaction';
    const SPLIT_LEVEL_SPLIT = 'split';


    const SCENARIO_INCOME_AND_EXPENSE = 'income-and-expense';
    const SCENARIO_SCHEDULE_C_WORKSHEET = 'schedule-c-worksheet';
    const SCENARIO_RECONCILIATION = 'reconciliation';
    const SCENARIO_MILEAGE = 'mileage';
    const SCENARIO_AUTO_ACTUAL_EXPENSE = 'auto-actual-expense';
    const SCENARIO_INVOICE_ACTIVITY = 'invoice-activity';
    const SCENARIO_INVOICE_ITEM_ACTIVITY = 'invoice-item-activity';
    const SCENARIO_INVOICE_SALES_TAX = 'invoice-sales-tax';
    const SCENARIO_INVOICE_AGING = 'invoice-aging';
    const SCENARIO_BUSINESS_ACTIVITY = 'business-activity';
    const SCENARIO_LOAN_ACTIVITY = 'loan-activity';

    public $to;
    public $from;
    public $output;
    public $transaction_level;
    public $split_level;
    public $type;
    public $account;
    public $auto;
    public $type_view;
    public $source;
    public $invoice_item;
    public $custom_tag;
    public $sales_tax;
    public $amount_from;
    public $amount_to;
    public $category;
    public $reconciled;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'to',
                    'from',
                ],
                'safe'
            ],
            [
                [
                    'to',
                    'from',
                    'output',
                    'transaction_level',
                    'split_level',
                    'type',
                    'account',
                    'type_view',
                    'reconciled'
                ],
                'required'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_INCOME_AND_EXPENSE => ['to', 'from', 'output'],
            self::SCENARIO_SCHEDULE_C_WORKSHEET => ['to', 'from', 'output'],
            self::SCENARIO_MILEAGE => ['to', 'from', 'type_view', 'output'],
            self::SCENARIO_AUTO_ACTUAL_EXPENSE => ['to', 'from', 'type_view', 'output'],
            self::SCENARIO_INVOICE_ACTIVITY => ['to', 'from', 'type_view', 'output'],
            self::SCENARIO_INVOICE_ITEM_ACTIVITY => ['to', 'from', 'type_view', 'output'],
            self::SCENARIO_INVOICE_SALES_TAX => ['to', 'from', 'type_view', 'output'],
            self::SCENARIO_INVOICE_AGING => ['to', 'from', 'type_view', 'output'],
            self::SCENARIO_BUSINESS_ACTIVITY => ['to', 'from', 'split_level', 'type_view', 'output'],
            self::SCENARIO_RECONCILIATION => ['account', 'reconciled', 'type_view', 'output'],
            self::SCENARIO_LOAN_ACTIVITY => ['to', 'from', 'account', 'type_view', 'output'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'report' => 'Select Report',
            'source' => 'Customer',
        ];
    }
}
