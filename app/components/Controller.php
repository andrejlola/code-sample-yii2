<?php
namespace app\components;

use Yii;
use yii\filters\AccessControl;
use common\models\backend\Order;
use yii\web\NotFoundHttpException;

/**
 * Parent Controller
 */
class Controller extends \yii\web\Controller
{
    public $relatedLinks = [];
    public $titlePage;

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($result = parent::beforeAction($action)) {
            if (!Yii::$app->user->isGuest && !Yii::$app->user->identity->isAgreementsAccepted()
                && $this->action->id != 'agreements'
            ) {
                return $this->redirect('agreements');
            }
        }
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAccountReadonlyMode()) {
            Yii::$app->session->setFlash('danger', 'Account is in Readonly mode');
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST
                    ? 'testme'
                    : null,
            ],
        ];
    }

    /**
     * Return Home Url for module
     *
     * @return mixed
     */
    public function getHomeUrl()
    {
        reset($this->relatedLinks);

        return key($this->relatedLinks);
    }

    /**
     * @return Order
     * @throws NotFoundHttpException
     */
    protected function findOrder()
    {
        if (($model = Order::findOne(['user_id' => Yii::$app->user->identity->getId()])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The order does not exist.');
        }
    }
}
