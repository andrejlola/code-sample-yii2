<?php

namespace app\components;

/**
 * Class for rendering PJAX modal form
 */
class PjaxModalForm
{
    /**
     * Return PJAX modal form
     *
     * @param $params
     * @return mixed
     */
    public static function get($params)
    {
        return \Yii::$app->controller->renderPartial('@app/views/layouts/_pjaxModalForm', $params);
    }
}
