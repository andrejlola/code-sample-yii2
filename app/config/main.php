<?php
$params = array_merge(require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'));

return [
    'id' => 'app',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\controllers',
    'modules' => [
        'auto' => [
            'class' => 'app\modules\auto\Module',
        ],
    ],
    'components' => [

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],

        'user' => [
            'identityClass' => 'common\models\backend\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG
                ? 3
                : 0,
            'targets' => [
                [
                    'class' => 'app\components\DbTarget',
                    'db' => 'app_db',
                    'logVars' => [],
                    'logTable' => 'log',
                    'levels' => [
                        'error',
                        'warning'
                    ],
                    'categories' => [
                        'app*',
                    ],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'auto' => 'auto/auto/index',
            ],
        ],
    ],
    'params' => $params,
];
