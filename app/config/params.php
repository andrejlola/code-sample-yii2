<?php
return [
    'adminEmail' => 'admin@example.com',
    'companyName' =>'finance.net',
    'supportEmail' =>'help@finance.net',
    'icon-framework' => 'fa',
    'DateFormatApplication' => 'd.m.Y',
    'DateTimeFormatApplication' => 'd.m.Y H:i',
    'DateFormatInternational' => 'Y-m-d',
    'DateTimeFormatInternational' => 'Y-m-d H:i:s',
];
