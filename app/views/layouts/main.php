<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$relatedLinks = $this->context->relatedLinks;
$infoWidget = $this->context->getInfoWidget();
?>

<?php
$this->beginContent('@app/views/layouts/sidebar.php');
?>

<?php if (count($relatedLinks) > 0 || count($infoWidget) > 0) { ?>
    <div class="sidebar">
        <?php if (count($relatedLinks) > 0) { ?>
            <div class="sidebar-header">
                <p>Related Activities</p>
            </div>

            <div class="sidebar-menu-widget">
                <ul>
                    <?php foreach ($relatedLinks as $route => $name) { ?>
                        <?php $isActive = false; ?>
                        <?php if (trim($route, '/') == Yii::$app->request->pathInfo) {
                            $this->title = $name;
                            $isActive = true;
                        } elseif ('/' . $this->context->module->id . '/' . $this->context->id == $route) {
                            $isActive = true;

                        }

                        if  ($this->title==null) {
                            if (isset($this->context->titlePage)) {
                                $this->title = $this->context->titlePage;
                            }
                        }


                        ?>
                        <li <?php echo $isActive
                            ? ' class="active"'
                            : '' ?>><a href="<?php echo Url::to([$route]) ?>"><?php echo $name ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        <?php }
              echo $this->render('parts/_sidebar_info_widget', [
                  'infoWidget' => $infoWidget
              ]);
        ?>
    </div>
<?php } ?>

<div class="content-body">
    <div class="content-header">
        <h2><?php echo $this->title ?></h2>

        <div class="login">
            <?php echo Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton('<i class="fa fa-sign-out" aria-hidden="true"></i> logout',
                ['class' => 'btn btn-default']) . Html::endForm() ?>
        </div>
    </div>
    <?php echo $content ?>
</div>

<?php $this->endContent(); ?>
