<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\ModalProcessing\ModalProcessing;
use \yii\bootstrap\Modal;

AppAsset::register($this);
$module = $this->context->module->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php if ($module != 'site') { ?>
        <?php $this->registerCssFile('@web/css/colors/' . $module . '.css', [
            'depends' => [
                \yii\bootstrap\BootstrapAsset::className(),
            ]
        ]); ?>
    <?php } ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="content">
        <header class="header-page-wrapper">
            <div class="container">
                <div class="row">
                    <div class="header-page">
                        <div class="logo-page">
                            <a href="/"><?php echo Html::img('@web/images/logo-KM_pages.png') ?></a>
                        </div>
                        <div class="menu-page">
                            <div class="menu-item <?php echo $module == 'transaction'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/transaction']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Enter Transaction</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'auto'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/auto']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Track Auto</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'invoice'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/invoice']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Track Invoices</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'account'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/account']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Balance Accounts</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'report'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/report']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Generate Reports</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'profile'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/profile']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Company Profile</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'communicate'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/communicate']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Communicate</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'practices'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/practices']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Explore Best Practices</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'service'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/service']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Premium Services</p>
                                    </div>
                                </a>
                            </div>
                            <div class="menu-item <?php echo $module == 'help'
                                ? 'active'
                                : '' ?>">
                                <a href="<?php echo Url::to(['/help']) ?>">
                                    <div class="menu-icon-wrapper">
                                        <div class="menu-icon"></div>
                                    </div>
                                    <div>
                                        <p>Get Help</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section>
            <div class="container">
                <div class="row">
                    <div class="content-page">
                        <?php echo Alert::widget() ?>
                        <?php echo ModalProcessing::widget(); ?>
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <?php
    $js = <<<JS
$.ajaxSetup({
    error : function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
            alert(jqXHR.responseText);
        } else {
            alert("Error: " + textStatus + ": " + errorThrown);
        }
    }
});
JS;
    $this->registerJs($js);
    ?>

    <footer>
        <div class="container">
            <div class="row">
                <p>© 2016 finance.net, LLC. All Rights Reserved.</p>
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
