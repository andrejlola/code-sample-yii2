<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\ModalProcessing\ModalProcessing;

AppAsset::register($this);
$module = $this->context->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="content">
        <header>
            <div class="container">
                <div class="row">
                    <div class="header-main">
                        <div class="col-md-6">
                            <a href="/"><?php echo Html::img('@web/images/logo-KM_main.png') ?></a>
                        </div>
                        <?php if (!Yii::$app->user->isGuest) { ?>
                            <div class="col-md-6">
                                <div class="login-wrapper">
                                    <div class="login">
                                        <p class="user-name">Welcome <?php echo Yii::$app->user->identity->getName() ?></p>
                                        <?php echo Html::beginForm(['/site/logout'], 'post')
                                        . Html::submitButton('<i class="fa fa-sign-out" aria-hidden="true"></i> logout',
                                            ['class' => 'btn btn-default']) . Html::endForm() ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </header>

        <section>
            <div class="container">
                <?php echo Alert::widget() ?>
                <?php echo ModalProcessing::widget(); ?>
                <?php echo $content ?>
        </section>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <p>© 2016 finance.net, LLC. All Rights Reserved.</p>
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
