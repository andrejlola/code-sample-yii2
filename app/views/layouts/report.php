<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;


AppAsset::register($this);
$module = $this->context->module->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->context->titlePage) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="content">
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text-center">Generated On: <b><?php echodate('m/d/Y');?></b></div>
                    <div class="col-md-3 text-center">Powered By: <b><?php echoYii::$app->params['companyName']?></b></div>
                </div>
                <div class="row">
                    <div class="content-page report">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
