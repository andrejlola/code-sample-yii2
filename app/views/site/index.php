<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'finance';
?>
<div class="row">
    <div class="main-content first-row">
        <div class="item-wrapper">
            <a href="<?php echo Url::to(['/auto']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Track Auto</h2>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="main-content second-row">
        <div class="item-wrapper">
            <a href="<?php echo Url::to(['/profile']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Company Profile</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?php echo Url::to(['/communicate']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Communicate</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?php echo Url::to(['/practices']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Explore Best Practices</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?php echo Url::to(['/service']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Premium Services</h2>
                    </div>
                </div>
            </a>
        </div>
        <div class="item-wrapper">
            <a href="<?php echo Url::to(['/help']) ?>">
                <div class="item-div">
                    <div class="item-icon">
                        <div class="icon"></div>
                    </div>
                    <div>
                        <h2>Get Help</h2>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
