<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->getCsrfToken() ?>"/>

            <?php echo $form->field($model, 'username')
                ->textInput(['autofocus' => true]) ?>

            <?php echo $form->field($model, 'password')
                ->passwordInput() ?>

            <?php
            $attribute = 'rememberMe';
            $id = Html::getInputId($model, $attribute);
            $name = Html::getInputName($model, $attribute);
            $value = Html::getAttributeValue($model, $attribute);
            $checked = $model->{$attribute} == true || $model->{$attribute} == 1;
            $label = $model->getAttributeLabel($attribute);
            echo $form->field($model, $attribute)
                ->checkbox([
                    'template' => '<div class="labeled">' . Html::checkbox($name, $checked, [
                            'id' => $id,
                            'value' => $value
                        ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>'
                ]) ?>

            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?php echo Html::a('reset it', ['site/request-password-reset']) ?>.
            </div>

            <div style="color:#999;margin:1em 0">
                Do not have account? <?php echo Html::a('Create it',
                    Yii::$app->urlManagerFront->createAbsoluteUrl('user/register')) ?>.
            </div>

            <div class="form-group">
                <?php echo Html::submitButton('Login', [
                    'class' => 'btn btn-primary',
                    'name' => 'login-button'
                ]) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
