<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%ZIPCODES}}".
 *
 * @property string $zip
 * @property string $city
 * @property string $state
 */
class ZipCode extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ZIPCODES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['zip'],
                'string',
                'max' => 16
            ],
            [
                [
                    'city',
                    'state'
                ],
                'string',
                'max' => 30
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zip' => 'Zip',
            'city' => 'City',
            'state' => 'State',
        ];
    }

    /**
     * Returns list of States
     *
     * @return mixed
     */
    public static function getStates()
    {
        $query = self::find()
            ->select('state as id, state')
            ->distinct()
            ->orderBy('state')
            ->asArray();

        $result = $query->all();

        return $result;
    }
}
