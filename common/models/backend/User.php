<?php

namespace common\models\backend;

use Yii;
use common\models\frontend\Preference;
use common\components\ActiveRecord;
use yii\base\NotSupportedException;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%USER}}".
 *
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $db_host
 * @property string $active
 * @property string $acct_type
 * @property string $reg_date
 * @property string $mailing_addr_id
 * @property string $billing_addr_id
 * @property string $beta
 * @property string $last_access
 * @property string $promo_code
 * @property string $reg_code
 * @property string $nanny_status
 * @property string $site
 * @property string $geocode
 *
 * @property Address $address
 * @property Message $sentMessages
 * @property Message $receivedMessages
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 'N';
    const STATUS_ACTIVE = 'Y';

    const ACCOUNT_TYPE_USER = 'U';

    const DEFAULT_BETA = 'P';
    const DEFAULT_REG_CODE = 'finance-STANDARD';
    const DEFAULT_SITE = 'finance';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%USER}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'active',
                'default',
                'value' => self::STATUS_ACTIVE
            ],
            [
                'active',
                'in',
                'range' => [
                    self::STATUS_ACTIVE,
                    self::STATUS_DELETED
                ]
            ],
            [
                [
                    'acct_type'
                ],
                'required'
            ],
            [
                [
                    'reg_date',
                    'last_access'
                ],
                'safe'
            ],
            [
                [
                    'nanny_status',
                    'site'
                ],
                'string'
            ],
            [
                [
                    'id',
                    'mailing_addr_id',
                    'billing_addr_id',
                    'reg_code'
                ],
                'string',
                'max' => 32
            ],
            [
                ['username'],
                'string',
                'max' => 35
            ],
            [
                ['password'],
                'string',
                'max' => 25
            ],
            [
                [
                    'db_host',
                    'promo_code'
                ],
                'string',
                'max' => 10
            ],
            [
                [
                    'active',
                    'acct_type',
                    'beta'
                ],
                'string',
                'max' => 1
            ],
            [
                ['geocode'],
                'string',
                'max' => 50
            ],
            [
                [
                    'username',
                    'password',
                    'site'
                ],
                'unique',
                'targetAttribute' => [
                    'username',
                    'password',
                    'site'
                ],
                'message' => 'The combination of Username, Password and Site has already been taken.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Login',
            'password' => 'Password',
            'db_host' => 'Db Host',
            'active' => 'Active',
            'acct_type' => 'Acct Type',
            'reg_date' => 'Reg Date',
            'mailing_addr_id' => 'Mailing Addr ID',
            'billing_addr_id' => 'Billing Addr ID',
            'beta' => 'Beta',
            'last_access' => 'Last Access',
            'promo_code' => 'Promo Code',
            'reg_code' => 'Reg Code',
            'nanny_status' => 'Nanny Status',
            'site' => 'Site',
            'geocode' => 'Geocode',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne([
            'id' => $id,
            'active' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username,
            'active' => self::STATUS_ACTIVE
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Hashes password
     *
     * @param $password
     * @return string
     */
    private function PasswordHash($password)
    {
        $nr = 1345345333;
        $add = 7;
        $nr2 = 0x12345671;
        $tmp = null;
        $inlen = strlen($password);
        for ($i = 0; $i < $inlen; $i++) {
            $byte = substr($password, $i, 1);
            if ($byte == ' ' || $byte == "\t") {
                continue;
            }
            $tmp = ord($byte);
            $nr ^= ((($nr & 63) + $add) * $tmp) + (($nr << 8) & 0xFFFFFFFF);
            $nr2 += (($nr2 << 8) & 0xFFFFFFFF) ^ $nr;
            $add += $tmp;
        }
        $out_a = $nr & ((1 << 31) - 1);
        $out_b = $nr2 & ((1 << 31) - 1);
        $output = sprintf("%08x%08x", $out_a, $out_b);

        return $output;
    }

    /**
     * Checks has the user already logged in
     *
     * @return bool
     */
    public function isFirstLogin()
    {
        return $this->last_access == '0000-00-00 00:00:00';
    }

    /**
     * Checks is user agreements accepted
     *
     * @return bool
     */
    public function isAgreementsAccepted()
    {
        if (($date = Preference::get(Preference::ACCEPT_LICENSE_AGREEMENT)) && $date != '0000-00-00') {
            return true;
        }

        return false;
    }

    /**
     * Accept user Agreements
     */
    public function acceptAgreements()
    {
        Preference::set(Preference::ACCEPT_LICENSE_AGREEMENT, Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd'));
    }

    /**
     * Checks is user account in readonly mode
     *
     * @return bool
     */
    public function isAccountReadonlyMode()
    {
        //todo
        return false;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (($result = $this->PasswordHash($password) == $this->password)) {
            if (!$this->isFirstLogin()) {
                $this->last_access = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');
                $this->save();
            }
        }

        return $result;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $this->PasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailingAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'mailing_addr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'billing_addr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSentMessages()
    {
        return $this->hasOne(Message::className(), ['id' => 'to_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivedMessages()
    {
        return $this->hasOne(Address::className(), ['id' => 'from_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->reg_date = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');
                $this->beta = self::DEFAULT_BETA;
                $this->reg_code = self::DEFAULT_REG_CODE;
                $this->site = self::DEFAULT_SITE;

                return true;
            } else {
            }
        }

        return true;
    }

    public function getName()
    {
        /** @var Order $order */
        $order = $this->getOrder()
            ->asArray()
            ->one();

        return $order['billing_firstname'] . ' ' . $order['billing_lastname'];
    }
}
