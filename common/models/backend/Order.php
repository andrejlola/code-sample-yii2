<?php

namespace common\models\backend;

use Yii;
use common\components\App;
use common\components\Stripe;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%ORDERS}}".
 *
 * @property integer $id
 * @property string $code
 * @property string $order_status
 * @property string $date_ordered
 * @property string $date_billed
 * @property string $date_modified
 * @property integer $package_id
 * @property integer $subscription_id
 * @property string $user_id
 * @property string $user_password
 * @property string $stripe_token
 * @property string $stripe_customer
 * @property integer $subscription_status
 * @property string $billing_status
 * @property string $billing_error
 * @property string $billing_startdate
 * @property integer $billing_recurrence
 * @property double $billing_initialcharge
 * @property string $billing_initial_reference
 * @property double $billing_recurringcharge
 * @property string $billing_recurring_reference
 * @property string $billing_method
 * @property string $billing_gateway
 * @property string $billing_cardname
 * @property integer $billing_cardpresent
 * @property string $billing_good_thru
 * @property integer $billing_update_info
 * @property string $billing_trackdata
 * @property string $billing_company
 * @property string $billing_firstname
 * @property string $billing_initial
 * @property string $billing_lastname
 * @property string $billing_address1
 * @property string $billing_address2
 * @property string $billing_city
 * @property string $billing_state
 * @property string $billing_zip
 * @property string $billing_phone
 * @property string $billing_mobile
 * @property string $billing_email
 * @property integer $shipping_different
 * @property string $shipping_address1
 * @property string $shipping_address2
 * @property string $shipping_city
 * @property string $shipping_state
 * @property string $shipping_zip
 * @property string $comments
 */
class Order extends \yii\db\ActiveRecord
{
    const SCENARIO_USER_REGISTER = 'SCENARIO_USER_REGISTER';
    const SCENARIO_COMPANY_UPDATE = 'SCENARIO_COMPANY_UPDATE';
    const SCENARIO_ADD_CARD = 'SCENARIO_ADD_CARD';

    const STATUS_NEW = 'NEW';
    const STATUS_PENDING = 'PENDING';
    const STATUS_ERROR = 'ERROR';
    const STATUS_COMPLETED = 'COMPLETED';
    const STATUS_MODIFIED = 'MODIFIED';
    const STATUS_CANCELLED = 'CANCELLED';

    const BILLING_STATUS_CANCELLED = 'CANCELLED';
    const BILLING_STATUS_DECLINED = 'DECLINED';
    const BILLING_STATUS_FRAUD = 'FRAUD';
    const BILLING_STATUS_APPROVED = 'APPROVED';

    const BILLING_BILLING_GATEWAY_STRIPE = 'Stripe';

    public $user_password_repeat;
    public $billing_cardtype;
    public $billing_cardnumber;
    public $billing_cardexp_month;
    public $billing_cardexp_year;
    public $billing_cardcvv;

    private $subscriptionObject;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ORDERS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'billing_email',
                    'user_password',
                    'user_password_repeat',
                    'billing_company',
                    'billing_firstname',
                    'billing_lastname',
                    'billing_address1',
                    'billing_city',
                    'billing_phone',
                    'billing_state',
                    'billing_zip',
                    'package_id',
                    'subscription_id',
                    'billing_cardtype',
                    'billing_cardnumber',
                    'billing_cardcvv',
                    'billing_cardexp_month',
                    'billing_cardexp_year',
                    'stripe_token'
                ],
                'required',
                'on' => self::SCENARIO_USER_REGISTER
            ],
            [
                [
                    'billing_cardtype',
                    'billing_cardnumber',
                    'billing_cardcvv',
                    'billing_cardexp_month',
                    'billing_cardexp_year',
                    'stripe_token'
                ],
                'required',
                'on' => self::SCENARIO_ADD_CARD
            ],
            [
                [
                    'billing_email',
                    'user_password',
                    'user_password_repeat',
                    'billing_firstname',
                    'billing_lastname',
                    'billing_address1',
                    'billing_city',
                    'billing_phone',
                    'billing_state',
                    'billing_zip',
                    'billing_initial',
                    'billing_address2',
                    'billing_company',
                    'billing_mobile',
                    'comments',
                ],
                'trim',
            ],
            [
                [
                    'billing_email',
                    'user_password',
                    'user_password_repeat',
                    'billing_firstname',
                    'billing_lastname',
                    'billing_address1',
                    'billing_city',
                    'billing_phone',
                    'billing_state',
                    'billing_zip',
                    'billing_initial',
                    'billing_address2',
                    'billing_company',
                    'billing_mobile',
                    'comments',
                ],
                'default',
            ],
            [
                'billing_email',
                'email'
            ],
            [
                'billing_email',
                'unique'
            ],
            [
                'user_password_repeat',
                'compare',
                'skipOnEmpty' => false,
                'compareAttribute' => 'user_password',
            ],
            [
                [
                    'billing_zip',
                    'shipping_zip'
                ],
                'exist',
                'targetClass' => 'common\models\backend\ZipCode',
                'targetAttribute' => 'zip'
            ],
            [
                'billing_cardtype',
                'in',
                'range' => array_keys(self::getCardTypes())
            ],
            [
                'billing_cardnumber',
                'frontend\components\CardNumberValidator',
            ],
            [
                ['billing_cardnumber'],
                'string',
                'max' => 28
            ],
            [
                ['billing_cardcvv'],
                'string',
                'min' => 3,
                'max' => 4
            ],
            [
                'subscription_id',
                'in',
                'range' => array_keys(ArrayHelper::map($this->getPackage()
                    ->one()
                    ->getSubscriptions()
                    ->all(), 'id', 'name'))
            ],
            [
                'package_id',
                'default',
                'value' => Package::DEFAULT_PACKAGE,
                'on' => self::SCENARIO_USER_REGISTER
            ],
            [
                'package_id',
                'exist',
                'targetClass' => 'common\models\backend\Package',
                'targetAttribute' => 'id',
            ],
            [
                [
                    'billing_initial',
                    'billing_address2',
                    //'billing_company',
                    'billing_mobile',
                    'comments',
                    'billing_initialcharge',
                    'billing_recurringcharge',
                    'billing_recurrence',
                    'billing_startdate',
                ],
                'safe',
                'on' => self::SCENARIO_USER_REGISTER
            ],
            [
                [
                    'billing_company',
                    'billing_firstname',
                    'billing_lastname',
                    'billing_address1',
                    'billing_city',
                    'billing_state',
                    'billing_zip',
                    'billing_phone',
                    'billing_email',
                ],
                'required',
                'on' => self::SCENARIO_COMPANY_UPDATE
            ],
            [
                [
                    'order_status',
                    'billing_status',
                    'billing_error',
                    'billing_method',
                    'billing_gateway',
                    'billing_trackdata',
                    'comments'
                ],
                'string'
            ],
            [
                [
                    'date_ordered',
                    'date_billed',
                    'date_modified',
                    'billing_startdate',
                    'billing_good_thru'
                ],
                'safe'
            ],
            [
                [
                    'package_id',
                    'subscription_id',
                    'subscription_status',
                    'billing_recurrence',
                    'billing_cardpresent',
                    'billing_update_info',
                    'shipping_different'
                ],
                'integer'
            ],
            [
                [
                    'billing_initialcharge',
                    'billing_recurringcharge'
                ],
                'number'
            ],
            [
                ['code'],
                'string',
                'max' => 20
            ],
            [
                [
                    'user_id',
                    'billing_cardname'
                ],
                'string',
                'max' => 32
            ],
            [
                ['user_password'],
                'string',
                'min' => 6,
                'max' => 15,
            ],
            [
                [
                    'stripe_token',
                    'stripe_customer'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'billing_initial_reference',
                    'billing_recurring_reference',
                    'billing_company',
                    'billing_firstname',
                    'billing_lastname',
                    'billing_address1',
                    'billing_address2',
                    'billing_city',
                    'billing_email',
                    'shipping_address1',
                    'shipping_address2',
                    'shipping_city'
                ],
                'string',
                'max' => 50
            ],
            [
                [
                    'billing_initial',
                    'billing_zip',
                    'shipping_zip'
                ],
                'string',
                'max' => 10
            ],
            [
                [
                    'billing_state',
                    'shipping_state'
                ],
                'string',
                'max' => 2
            ],
            [
                [
                    'billing_phone',
                ],
                'string',
                'length' => 14,
            ],
            [
                [
                    'billing_mobile'
                ],
                'string',
                'max' => 14
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Request #',
            'order_status' => 'Order Status',
            'date_ordered' => 'Date Ordered',
            'date_billed' => 'Date Billed',
            'date_modified' => 'Date Modified',
            'package_id' => 'Package',
            'subscription_id' => 'Subscription Type',
            'user_id' => 'User',
            'user_password' => 'User Password',
            'stripe_token' => 'Stripe Token',
            'stripe_customer' => 'Stripe Customer',
            'subscription_status' => 'Subscription Status',
            'billing_status' => 'Status',
            'billing_error' => 'Error',
            'billing_startdate' => 'Startdate',
            'billing_recurrence' => 'Recurrence',
            'billing_initialcharge' => 'Initialcharge',
            'billing_initial_reference' => 'Initial Reference',
            'billing_recurringcharge' => 'Recurringcharge',
            'billing_recurring_reference' => 'Recurring Reference',
            'billing_method' => 'Method',
            'billing_gateway' => 'Gateway',
            'billing_cardname' => 'Cardname',
            'billing_cardpresent' => 'Cardpresent',
            'billing_good_thru' => 'Good Thru',
            'billing_update_info' => 'Update Info',
            'billing_trackdata' => 'Trackdata',
            'billing_company' => 'Company',
            'billing_firstname' => 'First Name',
            'billing_initial' => 'Middle Initial',
            'billing_lastname' => 'Last Name',
            'billing_address1' => 'Address1',
            'billing_address2' => 'Address2',
            'billing_city' => 'City',
            'billing_state' => 'State',
            'billing_zip' => 'Zip',
            'billing_phone' => 'Phone',
            'billing_mobile' => 'Cell',
            'billing_email' => 'Email',
            'shipping_different' => 'Shipping Different',
            'shipping_address1' => 'Shipping Address1',
            'shipping_address2' => 'Shipping Address2',
            'shipping_city' => 'Shipping City',
            'shipping_state' => 'Shipping State',
            'shipping_zip' => 'Shipping Zip',
            'comments' => 'Comments',
            'user_password_repeat' => 'Retype Password',
            'billing_cardtype' => 'Card Type',
            'billing_cardnumber' => 'Card Number',
            'billing_cardexp_month' => 'Expiration',
            'billing_cardexp_year' => '',
            'billing_cardcvv' => 'CVV Number',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->code = $this->generateCode();
                $this->user_id = User::generateId();
                $this->date_ordered = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');

                return true;
            } else {
                $this->date_modified = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd HH:mm:ss');
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        OrderService::deleteAll(['order_id' => $this->id]);
        foreach ($this->getMySubscriptionServices() as $subscriptionService) {
            $subscriptionService->order_id = $this->id;
            $subscriptionService->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->user_password_repeat = $this->user_password;
    }

    /**
     * Returns type of billing card
     *
     * @return mixed
     */
    public function billingCardType()
    {
        return self::getCardTypes()[$this->billing_cardtype];
    }

    /**
     * Returns list of all card types
     *
     * @return array
     */
    public static function getCardTypes()
    {
        return [
            'Visa' => 'Visa',
            'MasterCard' => 'MasterCard',
            'AMEX' => 'American Express',
        ];
    }

    /**
     * Returns list of months
     *
     * @return array
     */
    public static function getCardMonth()
    {
        $month = [];
        for ($i = 1; $i <= 12; $i++) {
            $month[$i] = $i;
        }

        return $month;
    }

    /**
     * Return list of years
     *
     * @return array
     */
    public static function getCardYear()
    {
        $year = [];
        $curr_year = date('Y');
        for ($i = $curr_year; $i <= $curr_year + 9; $i++) {
            $year[$i] = $i;
        }

        return $year;
    }

    /**
     * Returns mask for card number
     *
     * @return string
     */
    public function getMaskedCardNumber()
    {
        $result = $this->billing_cardnumber;
        if (!is_null($this->billing_cardnumber) && strlen($this->billing_cardnumber)) {
            $parts = explode(' ', $this->billing_cardnumber);
            $parts[1] = str_replace(range(0, 9), '*', $parts[1]);
            $parts[2] = str_replace(range(0, 9), '*', $parts[2]);
            $result = implode(' ', $parts);
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderServices()
    {
        return $this->hasMany(OrderService::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->viaTable(OrderService::tableName(), [
                'order_id',
                'id'
            ]);
    }

    /**
     * Returns Package for current Order
     *
     * @return null|Package
     */
    public function getPackageObject()
    {
        return $this->getPackage()
            ->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }

    /**
     * Returns Subscription for current order
     *
     * @return Subscription
     */
    public function getSubscriptionObject()
    {
        if (is_null($this->subscriptionObject)) {
            $this->subscriptionObject = $this->getSubscription()
                ->one();
        }

        return $this->subscriptionObject;
    }

    /**
     * Return all subscription cases
     *
     * @return array
     */
    public function getSubscriptionPriceCases()
    {
        $result = [];
        /** @var Service[] $services */
        $services = $this->getPackageObject()
            ->getServices()
            ->where(['active' => 1])
            ->all();
        /** @var Subscription $subscription */
        $subscriptions = $this->getPackageObject()
            ->getSubscriptions()
            ->all();
        foreach ($subscriptions as $subscription) {
            $subscriptionCases = [];
            foreach ($services as $service) {
                $orderService = new OrderService();
                $orderService->service_id = $service->id;
                $orderService->price = $subscription->recurrence * $service->base_price;
                $orderService->base_discount = $subscription->discount;
                //todo coupon_discount
                $orderService->coupon_discount = 0;
                $subscriptionCases[] = $orderService;
            }
            $result[$subscription->id] = $subscriptionCases;
        }

        return $result;
    }

    /**
     * Returns current Subscription case
     *
     * @return OrderService[]
     */
    public function getMySubscriptionServices()
    {
        $allSubscriptionCases = $this->getSubscriptionPriceCases();

        return $allSubscriptionCases[$this->subscription_id];
    }

    /**
     * Returns total price of current subscription
     *
     * @return float|int
     */
    public function getMySubscriptionTotalBasePrice()
    {
        $mySubscriptionServices = $this->getMySubscriptionServices();

        return array_sum(ArrayHelper::getColumn($mySubscriptionServices, 'price'));
    }

    /**
     * Returns tota price with discount for current subscription
     *
     * @return float|int
     */
    public function getMySubscriptionTotalDiscountPrice()
    {
        $mySubscriptionServices = $this->getMySubscriptionServices();

        return array_sum(ArrayHelper::getColumn($mySubscriptionServices, function ($model) {
            return $model->getPriceWithDiscount();
        }));
    }

    /**
     * Confirms subscription
     *
     * @return bool
     */
    public function confirm()
    {
        try {
            $customer = Stripe::createCustomer($this->stripe_token, $this->subscription_id, $this->billing_email,
                $this->billing_firstname, $this->billing_lastname);
        } catch (\Stripe\Error\Base $ex) {
            Yii::error($ex->__toString(), 'payment');
            $this->billing_error = $ex->__toString();
            $this->order_status = self::STATUS_ERROR;
            $this->billing_status = self::BILLING_STATUS_DECLINED;
            $this->save();

            return false;
        }
        $customerData = json_decode(json_encode($customer), true);
        $this->subscription_status = 1;
        $this->stripe_customer = $customerData['id'];
        $this->billing_initial_reference = $customerData['subscriptions']['data'][0]['id'];
        $this->billing_recurring_reference = $customerData['id'];
        $this->order_status = self::STATUS_COMPLETED;
        $this->billing_status = self::BILLING_STATUS_APPROVED;
        $this->date_billed = date('Y-m-d H:i:s');
        $this->billing_error = '';
        $this->billing_recurrence = $this->getSubscriptionObject()->recurrence;
        $this->billing_startdate = (new \DateTime())->setTimestamp($customerData['subscriptions']['data'][0]['trial_end'])
            ->format('Y-m-d');
        $this->billing_initialcharge = $customerData['subscriptions']['data'][0]['plan']['amount'] / 100;
        $this->billing_recurringcharge = $customerData['subscriptions']['data'][0]['plan']['amount'] / 100;
        $this->billing_gateway = self::BILLING_BILLING_GATEWAY_STRIPE;
        if (!$this->save()) {
            Yii::error(var_export($this->errors, true), 'registration');

            return false;
        }

        return (new App)->initUser($this);
    }

    /**
     * Generates random code
     *
     * @return string
     */
    private function generateCode()
    {
        $guid = str_replace([
            '_',
            '-'
        ], '', Yii::$app->security->generateRandomString());
        $code = strtoupper(substr($guid, 0, 4) . '-' . substr($guid, 4, 4) . '-' . substr($guid, 8, 4) . '-'
            . substr($guid, 12, 4));

        return $code;
    }

    /**
     * Cancels current subscription
     */
    public function cancelSubscription()
    {
        $this->subscription_status = 0;
        $this->billing_initial_reference = null;
        $this->billing_recurring_reference = null;
        $this->billing_recurrence = 0;
        $this->billing_initialcharge = 0;
        $this->billing_recurringcharge = 0;
        $this->save();
    }
}
