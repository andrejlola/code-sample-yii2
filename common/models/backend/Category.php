<?php

namespace common\models\backend;

use Yii;
use common\models\frontend\Transaction;

/**
 * This is the model class for table "{{%CATEGORY}}".
 *
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $tax_form
 *
 * @property Transaction $transactions
 */
class Category extends \common\components\ActiveRecord
{
    const TYPE_INCOME = 'I';
    const TYPE_EXPENSE = 'E';
    const TYPE_OTHER = 'O';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%CATEGORY}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type'
                ],
                'required'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 75
            ],
            [
                ['type'],
                'string',
                'max' => 1
            ],
            [
                ['tax_form'],
                'string',
                'max' => 6
            ],
            [
                [
                    'name',
                    'type',
                    'tax_form'
                ],
                'unique',
                'targetAttribute' => [
                    'name',
                    'type',
                    'tax_form'
                ],
                'message' => 'The combination of Name, Type and Tax Form has already been taken.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'tax_form' => 'Tax Form',
        ];
    }

    /**
     * Returns list of allowed types
     *
     * @return array
     */
    public static function getCategoryTypes()
    {
        return [
            self::TYPE_INCOME => 'Income',
            self::TYPE_EXPENSE => 'Expense',
            self::TYPE_OTHER => 'Other',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['category_id' => 'id']);
    }

    /**
     * Returns all categories
     *
     * @return mixed
     */
    public static function getAll()
    {
        return self::find()
            ->orderBy('type asc, name asc')
            ->all();
    }

    /**
     * Returns all categories, grouped by type
     *
     * @return array
     */
    public static function getHierarchy()
    {
        $result = [];
        $categoryTypes = self::getCategoryTypes();
        foreach (self::getAll() as $item) {
            $typeName = $categoryTypes[$item->type];
            $e = [
                $item->id => $item->name
            ];
            if (array_key_exists($typeName, $result)) {
                $newItem = array_merge($result[$typeName], $e);
            } else {
                $newItem = $e;
            }
            $result[$typeName] = $newItem;
        }

        return $result;
    }
}
