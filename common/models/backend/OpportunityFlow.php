<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "OPP_FLOW".
 *
 * @property integer $id
 * @property string $name
 * @property string $opp_letter
 * @property string $description
 * @property string $create_date
 */
class OpportunityFlow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OPP_FLOW';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['opp_letter'], 'string', 'max' => 1],
            [['description'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'opp_letter' => 'Opp Letter',
            'description' => 'Description',
            'create_date' => 'Create Date',
        ];
    }

    /**
     * Returns all opportunity flows
     *
     * @return mixed
     */
    public static function getAll()
    {
        return self::find()
                ->orderBy('name')
                ->all();
    }
}
