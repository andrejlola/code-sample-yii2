<?php

namespace common\models\backend;

use yii\data\ActiveDataProvider;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends User
{
    public $first_name;
    public $last_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'username',
                    'active',
                    'last_access',
                    'nanny_status',
                    'acct_type',
                    'address.first_name',
                    'address.last_name',
                ],
                'safe'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'address.first_name',
            'address.last_name',
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->joinWith('address as address');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        if (!empty($this->username)) {
            $query->andFilterWhere([
                'like',
                'username',
                $this->getAttribute('username') . '%',
                false
            ]);
        }
        if (!empty($this->active)) {
            $query->andWhere('active=:active', ['active' => $this->active]);
        }
        if (!empty($this->acct_type)) {
            $query->andWhere('acct_type=:acct_type', ['acct_type' => $this->acct_type]);
        }
        if (!empty($this->getAttribute('address.first_name'))) {
            $query->andFilterWhere([
                'like',
                'address.first_name',
                $this->getAttribute('address.first_name') . '%',
                false
            ]);
        }
        if (!empty($this->getAttribute('address.last_name'))) {
            $query->andFilterWhere([
                'like',
                'address.last_name',
                $this->getAttribute('address.last_name') . '%',
                false
            ]);
        }

        return $dataProvider;
    }
}
