<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%PACKAGES_SUBSCRIPTIONS}}".
 *
 * @property integer $package_id
 * @property integer $subscription_id
 */
class PackageSubscription extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%PACKAGES_SUBSCRIPTIONS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'subscription_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'package_id' => 'Package ID',
            'subscription_id' => 'Subscription ID',
        ];
    }
}
