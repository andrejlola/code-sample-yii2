<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%TAX_TABLE}}".
 *
 * @property string $id
 * @property string $name
 * @property double $percent
 * @property string $def
 */
class TaxTable extends \common\components\AppActiveRecord
{
    const DEFAULT_TAX = 'Y';
    const NOT_DEFAULT_TAX = 'N';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TAX_TABLE}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'percent'],
                'required'
            ],
            [
                ['def'],
                'safe',
            ],
            [
                ['percent'],
                'number'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 60
            ],
            [
                ['def'],
                'string',
                'max' => 1
            ],
            [
                ['name'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'percent' => 'Percent',
            'def' => 'Default',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTax()
    {
        return $this->hasOne(InvoiceTax::className(), ['taxcode_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id'])
            ->via('invoiceTax');
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (!$this->canBeRenamedOrDeleted()) {
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        if (!self::find()
            ->where(['def' => self::DEFAULT_TAX])
            ->exists()
        ) {
            self::find()->one()->setAsDefault();
        }
    }

    /**
     * Returns active taxes sum
     *
     * @return mixed
     */
    public static function getActiveTaxesSum()
    {
        return self::find()->where(['def' => self::DEFAULT_TAX])->sum('percent');
    }

    /**
     * Sets current tax as default
     *
     * @return bool
     */
    public function setAsDefault()
    {
        $this->def = self::DEFAULT_TAX;

        return $this->save();
    }

    /**
     * Check is current tax default
     *
     * @return bool
     */
    public function isDefault()
    {
        return $this->def == self::DEFAULT_TAX;
    }

    /**
     * Checks can tax be renamed
     *
     * @return bool
     */
    public function canBeRenamedOrDeleted()
    {
        //todo
        return true;
    }

    /**
     * Returns models of taxes
     *
     * @return mixed
     */
    public static function getAll()
    {
        $query = self::find();

        return $query->all();
    }
}
