<?php

namespace common\models\frontend;

use Yii;
use common\components\Helper;

/**
 * This is the model class for table "{{%RECONCILE}}".
 *
 * @property string $id
 * @property string $account_id
 * @property string $date
 * @property string $ending_date
 * @property double $ending_balance
 * @property string $name
 * @property resource $cleared_items
 * @property resource $all_items
 * @property string $complete
 */
class Reconcile extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%RECONCILE}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'cleared_items',
                    'all_items'
                ],
                'required'
            ],
            [
                [
                    'date',
                    'ending_date'
                ],
                'safe'
            ],
            [
                ['ending_balance'],
                'number'
            ],
            [
                [
                    'cleared_items',
                    'all_items'
                ],
                'string'
            ],
            [
                [
                    'id',
                    'account_id'
                ],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 255
            ],
            [
                ['complete'],
                'string',
                'max' => 1
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'date' => 'Date',
            'ending_date' => 'Ending Date',
            'ending_balance' => 'Ending Balance',
            'name' => 'Name',
            'cleared_items' => 'Cleared Items',
            'all_items' => 'All Items',
            'complete' => 'Complete',
        ];
    }

    /**
     * Returns last reconcile model
     *
     * @param null $year
     * @return mixed
     */
    public static function getLast($year = null)
    {
        $query = self::find();
        $year = is_null($year) ? Helper::getCurrentYearDate() : $year;
        $year = $year . ' 00:00:00';
        $query->andFilterWhere([
            '=',
            'date',
            $year
        ]);
        $query->andFilterWhere([
            '=',
            'complete',
            'Y'
        ]);

        return $query->max('ending_date');
    }

}
