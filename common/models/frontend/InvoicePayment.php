<?php

namespace common\models\frontend;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%INVOICE_PAYMENTS}}".
 *
 * @property string $id
 * @property string $invoice_id
 * @property string $date_entered
 * @property string $date
 * @property string $method
 * @property string $desc
 * @property string $payment_id
 * @property double $amount
 */
class InvoicePayment extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%INVOICE_PAYMENTS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'amount',
                    'date'
                ],
                'required'
            ],
            [
                [
                    'date_entered',
                    'date'
                ],
                'safe'
            ],
            [
                ['amount'],
                'number'
            ],
            [
                [
                    'id',
                    'invoice_id',
                    'method'
                ],
                'string',
                'max' => 32
            ],
            [
                ['desc'],
                'string',
                'max' => 255
            ],
            [
                ['payment_id'],
                'string',
                'max' => 20
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'date_entered' => 'Date Entered',
            'date' => 'Date',
            'method' => 'Method',
            'desc' => 'Desc',
            'payment_id' => 'Payment ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->hasAttribute('date')) {
                    $this->date = new Expression('NOW()');
                }
            }
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave()
    {
        Invoice::setStatus($this->invoice_id);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        Invoice::setStatus($this->invoice_id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}
