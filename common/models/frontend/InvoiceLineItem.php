<?php

namespace common\models\frontend;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%INVOICE_LINE_ITEMS}}".
 *
 * @property string $id
 * @property string $invoice_id
 * @property string $income_item_id
 * @property integer $position
 * @property string $date_added
 * @property double $qty
 * @property string $modifier
 * @property string $item
 * @property string $desc
 * @property string $tax
 * @property double $price_each
 */
class InvoiceLineItem extends \common\components\AppActiveRecord
{
    public $total_sum_no_tax = 0;
    public $total_sum_tax = 0;
    public $total_sum_with_tax = 0;
    public $item_name = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%INVOICE_LINE_ITEMS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'invoice_id',
                    'income_item_id',
                    'price_each',
                    'qty',
                ],
                'required'
            ],
            [
                ['position'],
                'integer'
            ],
            [
                ['date_added'],
                'safe'
            ],
            [
                [
                    'qty',
                    'price_each'
                ],
                'number'
            ],
            [
                [
                    'id',
                    'invoice_id',
                    'income_item_id',
                    'modifier'
                ],
                'string',
                'max' => 32
            ],
            [
                ['item'],
                'string',
                'max' => 75
            ],
            [
                ['desc'],
                'string',
                'max' => 255
            ],
            [
                ['tax'],
                'string',
                'max' => 1
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'income_item_id' => 'Invoice Item',
            'position' => 'Position',
            'date_added' => 'Date Added',
            'qty' => 'Quantity',
            'modifier' => 'Modifier',
            'item' => 'Item',
            'desc' => 'Desc',
            'tax' => 'Tax',
            'price_each' => 'Price Each',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->hasAttribute('date_added')) {
                    $this->date_added = new Expression('NOW()');
                }
                if (array_key_exists('id', $this->getAttributes()) && is_null($this->id)) {
                    $this->id = self::generateId();
                }
            }
            if ($this->hasAttribute('tax')) {
                if ($this->tax == '1') {
                    $this->tax = 'T';
                } else {
                    $this->tax = 'F';
                };
            }
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave()
    {
        Invoice::setStatus($this->invoice_id);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        Invoice::setStatus($this->invoice_id);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->tax == 'T') {
            $this->tax = '1';
        } else {
            $this->tax = '0';
        }

        parent::afterFind();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceItem()
    {
        return $this->hasOne(InvoiceItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * Checks is invoice line item taxable
     *
     * @return bool
     */
    public function isTaxable()
    {
        return $this->tax == '1';
    }
}
