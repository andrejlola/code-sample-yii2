<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%BANK_ACCOUNTS}}".
 *
 * @property string $id
 * @property string $name
 * @property string $acct_type
 * @property double $begin_balance
 * @property double $balance
 * @property string $last_reconciled
 * @property string $active
 *
 * @property Transaction[] $transactions
 */
class BankAccount extends \common\components\AppActiveRecord
{
    const STATUS_DELETED = 'N';
    const STATUS_ACTIVE = 'Y';

    const TYPE_BUSINESS = 'B';
    const TYPE_CREDIT = 'C';
    const TYPE_LOAN = 'L';
    const TYPE_ASSET = 'A';

    public $total_balance;
    public $total_in;
    public $total_out;
    public $account_id;
    public $sum_total_balance;
    public $sum_total_in;
    public $sum_total_out;
    public $group;
    public $name_total_balance;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%BANK_ACCOUNTS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'acct_type',
                    'active'
                ],
                'required'
            ],
            [
                [
                    'begin_balance',
                    'balance'
                ],
                'number'
            ],
            [
                ['last_reconciled'],
                'safe'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 35
            ],
            [
                [
                    'acct_type',
                    'active'
                ],
                'string',
                'max' => 1
            ],
            [
                ['name'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Account Name',
            'acct_type' => 'Account Type',
            'begin_balance' => 'Begin Balance',
            'balance' => 'Balance',
            'last_reconciled' => 'Last Reconciled',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasMany(Transaction::className(), ['account_id' => 'id']);
    }

    /**
     * Returns models list of bank accounts
     *
     * @param bool $showInactive
     * @return mixed
     */
    public static function getAll($showInactive = false)
    {
        $query = self::find();
        if (!$showInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }

        return $query->all();
    }

    /**
     * Returns all loans accounts
     *
     * @return mixed
     */
    public static function getLoansAll()
    {
        $query = self::find()
            ->where(['acct_type' => ['L', 'A']]);

        return $query->all();
    }

    /**
     * Calculates all totals
     *
     * @param bool $showInactive
     * @return mixed
     */
    public static function getAllTotal($showInactive = false)
    {
        $query = self::find()
            ->select([
                '{{BANK_ACCOUNTS}}.id',
                ' CONCAT(name, ": $", cast(sum(ifnull({{TRANSACTION}}.amount, 0)) as  char )) as name_total_balance ',
                '{{BANK_ACCOUNTS}}.acct_type as group',
            ])
            ->joinWith('transaction');
        if (!$showInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }
        $query->groupBy('{{BANK_ACCOUNTS}}.id');

        $data = $query->all();

        foreach ($data as $key => $item) {
            $val = self::getTypes([$data[$key]['group']]);
            $data[$key]['group'] = $val[$data[$key]['group']];
        }
        return $data;
    }

    /**
     * Calculates balance for account
     *
     * @param $account_id
     * @return int
     */
    public static function getAccountBalance($account_id)
    {
        $query = self::find()->where(['{{TRANSACTION}}.account_id' => $account_id]);
        $total = $query->joinWith('transaction')
            ->sum('{{TRANSACTION}}.amount');

        return is_null($total)
            ? 0
            : $total;
    }

    /**
     * Calculates total balances
     *
     * @param bool $includeInactive
     * @return int
     */
    public static function getTotalBalance($includeInactive = false)
    {
        $query = self::find();
        if (!$includeInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }
        $total = $query->joinWith('transaction')
            ->sum('{{TRANSACTION}}.amount');

        return is_null($total)
            ? 0
            : $total;

    }

    /**
     * Calculates sum total
     *
     * @param bool $includeInactive
     * @return mixed
     */
    public static function getSumTotal($includeInactive = false)
    {
        $query = self::find()
            ->select([
                'sum(ifnull({{TRANSACTION}}.amount, 0)) as sum_total_balance',
                'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)>=0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as sum_total_in',
                'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)<0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as sum_total_out'
            ])
            ->joinWith('transaction');
        if (!$includeInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }

        return $query->one();
    }

    /**
     * Calculates sum loans total
     *
     * @param bool $includeInactive
     * @return mixed
     */
    public static function getSumLoansTotal($includeInactive = false)
    {
        $query = self::find()
            ->select([
                'sum(ifnull({{TRANSACTION}}.amount, 0)) as sum_total_balance',
                'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)>=0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as sum_total_in',
                'sum( CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)<0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as sum_total_out'
            ])
            ->joinWith('transaction');
        if (!$includeInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }
        $query->where(['acct_type' => ['L', 'A']]);

        return $query->one();
    }

    /**
     * Calculates loans total by type
     *
     * @param bool $includeInactive
     * @return mixed
     */
    public static function getSumLoansByTypeTotal($includeInactive = false)
    {
        $query = self::find()
            ->select([
                'sum( CASE  WHEN acct_type=\'A\' THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as sum_total_in',
                'sum( CASE  WHEN acct_type=\'L\' THEN  ifnull({{TRANSACTION}}.amount,0) ELSE 0 END ) as sum_total_out'
            ])
            ->joinWith('transaction');
        if (!$includeInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }
        $query->where(['acct_type' => ['L', 'A']]);
        return $query->one();
    }

    /**
     * Returns list of bank account types
     *
     * @param array $filter
     * @return array
     */
    public static function getTypes(array $filter = [])
    {
        $allTypes = [
            self::TYPE_BUSINESS => 'Bank Account (+)',
            self::TYPE_CREDIT => 'Credit Card (-)',
            self::TYPE_LOAN => 'Loan',
            self::TYPE_ASSET => 'Asset',
        ];
        if (count($filter)) {
            $result = array_filter($allTypes, function ($key) use ($filter) {
                return in_array($key, $filter);
            }, ARRAY_FILTER_USE_KEY);
        } else {
            $result = $allTypes;
        }

        return $result;
    }

    /**
     *Returns list of loan types
     *
     * @param array $filter
     * @return array
     */
    public static function getLoanTypes(array $filter = [])
    {
        $allTypes = [
            self::TYPE_ASSET => 'Asset',
            self::TYPE_LOAN => 'Liability'
        ];
        if (count($filter)) {
            $result = array_filter($allTypes, function ($key) use ($filter) {
                return in_array($key, $filter);
            }, ARRAY_FILTER_USE_KEY);
        } else {
            $result = $allTypes;
        }

        return $result;
    }

}
