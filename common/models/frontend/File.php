<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%FILE}}".
 *
 * @property integer $id
 * @property string $datatype
 * @property string $purpose
 * @property integer $parent_id
 * @property string $name
 * @property string $size
 * @property string $filedate
 */
class File extends \common\components\AppActiveRecord
{
    const LOGO_PURPOSE = 'invoiceLogo';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%FILE}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'parent_id',
                    'size'
                ],
                'integer'
            ],
            [
                ['filedate'],
                'safe'
            ],
            [
                ['datatype'],
                'string',
                'max' => 60
            ],
            [
                ['purpose'],
                'string',
                'max' => 50
            ],
            [
                ['name'],
                'string',
                'max' => 120
            ],

            [
                [
                    'datatype',
                    'purpose',
                    'name',
                    'parent_id'
                ],
                'unique',
                'targetAttribute' => [
                    'datatype',
                    'purpose',
                    'name',
                    'parent_id'
                ],
                'message' => 'The combination of Datatype, Purpose, Parent ID and Name has already been taken.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datatype' => 'Datatype',
            'purpose' => 'Purpose',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'size' => 'Size',
            'filedate' => 'Filedate',
        ];
    }

    /**
     * Deletes current file
     *
     * @return bool
     */
    public function deleteFile()
    {
        $fileData = $this->getFileData()
            ->one();
        if ($fileData->delete()) {
            $this->delete();

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileData()
    {
        return $this->hasOne(FileData::className(), ['masterid' => 'id']);
    }
}
