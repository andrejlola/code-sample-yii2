<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%CUSTOM_TAGS}}".
 *
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $active
 *
 * @property Transaction[] $transactions
 */
class CustomTag extends \common\components\AppActiveRecord
{
    const STATUS_DELETED = 'N';
    const STATUS_ACTIVE = 'Y';

    const TYPE_E = 'E';
    const TYPE_I = 'I';
    const TYPE_B = 'B';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%CUSTOM_TAGS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                ],
                'required'
            ],
            [
                ['id'],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 25
            ],
            [
                [
                    'type',
                    'active'
                ],
                'string',
                'max' => 1
            ],
            [
                [
                    'name',
                    'type'
                ],
                'unique',
                'targetAttribute' => [
                    'name',
                    'type'
                ],
                'message' => 'The combination of Name and Type has already been taken.'
            ],
            [
                'active',
                'default',
                'value' => self::STATUS_ACTIVE
            ],
            [
                'type',
                'default',
                'value' => self::TYPE_B
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasMany(Transaction::className(), ['custom_tag_id' => 'id']);
    }

    /**
     * Returns models list of custom tags
     *
     * @param bool $showInactive
     * @return mixed
     */
    public static function getAll($showInactive = false)
    {
        $query = self::find();
        if (!$showInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }

        return $query->all();
    }
}
