<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%IMPORTED_ITEMS}}".
 *
 * @property string $id
 * @property string $type
 * @property string $subtype
 * @property string $status
 * @property string $source
 * @property string $code
 * @property string $date_imported
 * @property string $date_ignored
 * @property string $date_processed
 * @property resource $data
 * @property integer $data_version
 * @property string $confidence_level
 * @property string $acct
 * @property string $transaction_id
 */
class ImportedItem extends \common\components\AppActiveRecord
{
    const TYPE_ITEM = 'ITEM';
    const TYPE_ORDER = 'ORDER';
    const TYPE_INVOICE = 'INVOICE';
    const TYPE_FEE_CREDIT = 'FEE/CREDIT';
    const TYPE_INVESTMENT = 'INVESTMENT';
    const TYPE_UNDEFINED = 'UNDEFINED';

    const SUBTYPE_I = 'I';
    const SUBTYPE_E = 'E';
    const SUBTYPE_O = 'O';

    const STATUS_PENDING = 'PENDING';
    const STATUS_IGNORED = 'IGNORED';
    const STATUS_PROCESSED = 'PROCESSED';

    const SOURCE_EBAY = 'EBAY';
    const SOURCE_AUTHNET = 'AUTHNET';
    const SOURCE_PAYPAL = 'PAYPAL';
    const SOURCE_FILE = 'FILE';
    const SOURCE_UNDEFINED = 'UNDEFINED';

    const CONFIDENCE_LEVEL_GREEN = 'GREEN';
    const CONFIDENCE_LEVEL_YELLOW = 'YELLOW';
    const CONFIDENCE_LEVEL_RED = 'RED';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%IMPORTED_ITEMS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'data'
                ],
                'required'
            ],
            [
                [
                    'type',
                    'subtype',
                    'status',
                    'source',
                    'data',
                    'confidence_level'
                ],
                'string'
            ],
            [
                [
                    'date_imported',
                    'date_ignored',
                    'date_processed'
                ],
                'safe'
            ],
            [
                ['data_version'],
                'integer'
            ],
            [
                [
                    'id',
                    'transaction_id'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'code',
                    'acct'
                ],
                'string',
                'max' => 50
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'subtype' => 'Subtype',
            'status' => 'Status',
            'source' => 'Source',
            'code' => 'Code',
            'date_imported' => 'Date Imported',
            'date_ignored' => 'Date Ignored',
            'date_processed' => 'Date Processed',
            'data' => 'Data',
            'data_version' => 'Data Version',
            'confidence_level' => 'Confidence Level',
            'acct' => 'Acct',
            'transaction_id' => 'Transaction ID',
        ];
    }

    /**
     * Returns list of possible types
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_ITEM => self::TYPE_ITEM,
            self::TYPE_ORDER => self::TYPE_ORDER,
            self::TYPE_INVOICE => self::TYPE_INVOICE,
            self::TYPE_FEE_CREDIT => self::TYPE_FEE_CREDIT,
            self::TYPE_INVESTMENT => self::TYPE_INVESTMENT,
            self::TYPE_UNDEFINED => self::TYPE_UNDEFINED,
        ];
    }

    /**
     * Returns list of possible subtypes
     *
     * @return array
     */
    public static function getSubtypes()
    {
        return [
            self::SUBTYPE_I => self::SUBTYPE_I,
            self::SUBTYPE_O => self::SUBTYPE_O,
            self::SUBTYPE_E => self::SUBTYPE_E,
        ];
    }

    /**
     * Returns list of possible statuses
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_PENDING => self::STATUS_PENDING,
            self::STATUS_IGNORED => self::STATUS_IGNORED,
            self::STATUS_PROCESSED => self::STATUS_PROCESSED,
        ];
    }

    /**
     * Returns list of possible sources
     *
     * @return array
     */
    public static function getSources()
    {
        return [
            self::SOURCE_EBAY => self::SOURCE_EBAY,
            self::SOURCE_AUTHNET => self::SOURCE_AUTHNET,
            self::SOURCE_PAYPAL => self::SOURCE_PAYPAL,
            self::SOURCE_FILE => self::SOURCE_FILE,
            self::SOURCE_UNDEFINED => self::SOURCE_UNDEFINED,
        ];
    }

    /**
     * Returns list of possible confidence levels
     *
     * @return array
     */
    public static function getConfidenceLevels()
    {
        return [
            self::CONFIDENCE_LEVEL_RED => self::CONFIDENCE_LEVEL_RED,
            self::CONFIDENCE_LEVEL_YELLOW => self::CONFIDENCE_LEVEL_YELLOW,
            self::CONFIDENCE_LEVEL_GREEN => self::CONFIDENCE_LEVEL_GREEN,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'id']);
    }
}
