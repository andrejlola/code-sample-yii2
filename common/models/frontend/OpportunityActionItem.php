<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%OPP_ACTION_ITEM}}".
 *
 * @property string $id
 * @property string $opp_action
 * @property string $create_date
 * @property string $action_date
 * @property string $from_task
 * @property string $task
 * @property resource $data
 */
class OpportunityActionItem extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%OPP_ACTION_ITEM}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'data'
                ],
                'required'
            ],
            [
                [
                    'create_date',
                    'action_date'
                ],
                'safe'
            ],
            [
                ['data'],
                'string'
            ],
            [
                [
                    'id',
                    'opp_action',
                    'from_task',
                    'task'
                ],
                'string',
                'max' => 32
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'opp_action' => 'Opp Action',
            'create_date' => 'Create Date',
            'action_date' => 'Action Date',
            'from_task' => 'From Task',
            'task' => 'Task',
            'data' => 'Data',
        ];
    }
}
