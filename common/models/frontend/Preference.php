<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%PREF}}".
 *
 * @property string $pref_key
 * @property string $pref_value
 * @property string $pref_userid
 */
class Preference extends \common\components\AppActiveRecord
{
    const VIEW_FIRST_AUTO = 'view_first_auto';
    const VIEW_FIRST_TRANSACTIONS = 'view_first_transactions';
    const VIEW_FIRST_BALANCE = 'view_first_balance';
    const VIEW_FIRST_INVOICE = 'view_first_invoice';
    const RECENT_AUTO = 'recent_auto';
    const RECENT_ACCOUNT = 'recent_account';
    const AUTH_ACCOUNTANT = 'auth_accountant';
    const BUSINESS_TYPE = 'business_type';
    const BUSINESS_ENTITY = 'business_entity';
    const EXISTING_BUSINESS = 'existing_business';
    const DATE_STARTED = 'date_started';
    const INVENTORY_PREF = 'inventory_pref';
    const INVOICE_PREF = 'invoice_pref';
    const COLLECT_SALES_TAX = 'collect_sales_tax';
    const SALES_TAX_RATE = 'sales_tax_rate';
    const PREPARE_1099 = 'prepare_1099';
    const EMPLOYEES = 'employees';
    const NUMBER_EMPLOYEES = 'number_employees';
    const TAB_OPTION = 'tab_option';
    const LOGOUT_TIME = 'logout_time';
    const GRX_LOC = 'grx_loc';
    const ACCEPT_LICENSE_AGREEMENT = 'accept_license_agreement';
    const CHOSE_PATH = 'chose_path';
    const GREETING = 'greeting';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%PREF}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'pref_key',
                    'pref_userid'
                ],
                'required'
            ],
            [
                ['pref_value'],
                'string'
            ],
            [
                ['pref_value'],
                'default',
                'value' => ''
            ],
            [
                [
                    'pref_key',
                    'pref_userid'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'pref_key',
                    'pref_userid'
                ],
                'unique',
                'targetAttribute' => [
                    'pref_key',
                    'pref_userid'
                ],
                'message' => 'The combination of Pref Key and Pref Userid has already been taken.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pref_key' => 'Pref Key',
            'pref_value' => 'Pref Value',
            'pref_userid' => 'Pref Userid',
        ];
    }

    /**
     * Sets an preference
     *
     * @param $key
     * @param $value
     * @param null $userId
     */
    public static function set($key, $value, $userId = null)
    {
        $params = ['pref_key' => $key];
        if ($userId) {
            $params['pref_userid'] = $userId;
        }
        if (!($model = self::findOne($params))) {
            $model = new self();
            $model->pref_key = $key;
            if ($userId) {
                $model->pref_userid = $userId;
            } else {
                $model->pref_userid = Yii::$app->user->id;
            }
        }
        $model->pref_value = $value;
        $model->save();
    }

    /**
     * Gets an preference
     *
     * @param $key
     * @param null $userId
     * @return bool
     */
    public static function get($key, $userId = null)
    {
        $params = ['pref_key' => $key];
        if ($userId) {
            $params['pref_userid'] = $userId;
        }
        if (($model = self::findOne($params))) {
            return $model->pref_value;
        }

        return false;
    }
}
