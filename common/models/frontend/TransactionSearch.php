<?php

namespace common\models\frontend;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the ActiveQuery class for [[Transaction]].
 *
 * @see Transaction
 */
class TransactionSearch extends Transaction
{
    public $dateFrom;
    public $dateTo;
    public $funds_in;
    public $funds_out;
    public $funds_in_total;
    public $funds_out_total;
    public $source_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'dateFrom',
                ],
                'default',
                'value' => (new \DateTime())->sub(new \DateInterval('P1M'))
                    ->format('Y-m-d')
            ],
            [
                [
                    'dateTo',
                ],
                'default',
                'value' => (new \DateTime())->format('Y-m-d')
            ],
            [
                [
                    'id',
                    'type',
                    'date',
                    'date_entered',
                    'date_modified',
                    'reconciled_date',
                    'locked_date',
                    'approved_date',
                    'amount',
                    'account_id',
                    'source_id',
                    'method_id',
                    'category_id',
                    'custom_tag_id',
                    'master_id',
                    'auto_id',
                    'accountant_changed',
                    'type',
                    'split',
                    'inventory',
                    'auto',
                    'reconciled',
                    'locked',
                    'approved',
                    'dateFrom',
                    'dateTo',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->dateFrom = $this->toStorageDate($this->dateFrom);
        $this->dateTo = $this->toStorageDate($this->dateTo);

        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params, $expense = false)
    {
        $query = self::find()
            ->select([
                '{{TRANSACTION}}.*',
                'CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)>=0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE null END  as funds_in',
                'CASE  WHEN ifnull({{TRANSACTION}}.amount, 0)<0 THEN  ifnull({{TRANSACTION}}.amount,0) ELSE null END  as funds_out',
                'ifnull({{SOURCE}}.name, {{TRANSACTION}}.source_id) as source_name '
            ])
            ->joinWith('bankAccount')
            ->joinWith('source')
            ->with('category')
            ->orderBy(['date' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        if (!is_null($this->dateFrom)) {
            $query->andFilterWhere([
                '>=',
                'date',
                $this->dateFrom
            ]);
        }
        if (!is_null($this->dateTo)) {
            $query->andFilterWhere([
                '<=',
                'date',
                $this->dateTo
            ]);
        }
        if (!is_null($this->dateTo)) {
            $query->andFilterWhere([
                '=',
                'account_id',
                $this->account_id
            ]);
        }
        if (!is_null($this->auto_id)) {
            $query->andWhere(['{{TRANSACTION}}.auto_id' => $this->auto_id]);
        }

        if ($expense) {
            $query->andWhere(['{{TRANSACTION}}.category_id' => 'fa2f21f556984750e5954a9da01315b7']);
        }

        $this->dateFrom = $this->toAppDate($this->dateFrom);
        $this->dateTo = $this->toAppDate($this->dateTo);

        $this->funds_in_total = $query->sum('CASE  WHEN ifnull(amount, 0)>=0 THEN  ifnull(amount,0) ELSE null END ');
        $this->funds_out_total = $query->sum('CASE  WHEN ifnull(amount, 0)<0 THEN  ifnull(amount,0) ELSE null END ');

        return $dataProvider;
    }


}
