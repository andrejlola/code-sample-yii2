<?php

namespace common\models\frontend;

use Yii;
use yii\db\Connection;

class AccountantClientAffiliate extends AccountantClient
{
    public static $userDBName;

    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        /** @var Connection $connection */
        $connection = Yii::$app->app_db;
        $connection->dsn = str_replace('{dbname}', self::$userDBName, $connection->dsn);

        return $connection;
    }
}
