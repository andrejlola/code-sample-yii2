<?php

namespace common\models\frontend;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%INVOICES}}".
 *
 * @property string $id
 * @property string $date
 * @property string $inv_number
 * @property string $inv_date
 * @property string $due_date
 * @property string $source_id
 * @property string $po_number
 * @property integer $terms
 * @property string $cust_terms
 * @property resource $bill_addr
 * @property resource $ship_addr
 * @property double $tax_rate
 * @property integer $status
 * @property resource $notes
 */
class Invoice extends \common\components\AppActiveRecord
{
    public $inv_total = 0;
    public $sales_tax;
    public $total_sum_without_tax = 0;
    public $total_sum_tax = 0;
    public $total_sum_with_tax = 0;
    public $total_sum_payment = 0;
    public $customer_name = '';
    public $max_date = '';
    public $min_date = '';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%INVOICES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'inv_date',
                    'terms',
                    'inv_number',
                    'source_id',
                    'due_date'
                ],
                'required'
            ],
            [
                [
                    'date',
                    'inv_date',
                    'due_date'
                ],
                'safe'
            ],
            [
                [
                    'terms',
                    'status'
                ],
                'integer'
            ],
            [
                [
                    'bill_addr',
                    'ship_addr',
                    'notes'
                ],
                'string'
            ],
            [
                ['tax_rate'],
                'number'
            ],
            [
                [
                    'id',
                    'source_id'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'inv_number',
                    'po_number'
                ],
                'string',
                'max' => 20
            ],
            [
                ['cust_terms'],
                'string',
                'max' => 75
            ],
            [
                ['inv_number'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'inv_number' => 'Invoice Number',
            'inv_date' => 'Invoice Date',
            'due_date' => 'Due Date',
            'source_id' => 'Customer',
            'po_number' => 'Purchase Order Number',
            'terms' => 'Terms',
            'cust_terms' => 'Cust Terms',
            'bill_addr' => 'Bill Addr',
            'ship_addr' => 'Ship Addr',
            'tax_rate' => 'Tax Rate',
            'status' => 'Status',
            'notes' => 'Notes',
            'inv_total' => 'Total',
            'payment_total' => 'Payments',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTax()
    {
        return $this->hasOne(InvoiceTax::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxTable()
    {
        return $this->hasOne(TaxTable::className(), ['id' => 'taxcode_id'])
            ->via('invoiceTax');
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->hasAttribute('date')) {
                    $this->date = new Expression('NOW()');
                }
                if ($this->hasAttribute('tax_rate')) {
                    $this->tax_rate = TaxTable::getActiveTaxesSum();
                }
                $this->status = -2;
            }

            return true;
        }

        return false;
    }

    /**
     * Sets status for invoice
     *
     * @param $id
     */
    public function setStatus($id)
    {
        $invoice = Invoice::find()
            ->select([
                '{{INVOICES}}.*',
                'round(sum({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty),2) as total_sum_without_tax',
                'sum(CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*{{INVOICES}}.tax_rate)/100,2) ELSE 0 END)  as total_sum_tax',
                'sum(CASE  WHEN {{INVOICE_LINE_ITEMS}}.tax = \'T\' THEN  round(({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty*(100+{{INVOICES}}.tax_rate))/100,2) ELSE round({{INVOICE_LINE_ITEMS}}.price_each * {{INVOICE_LINE_ITEMS}}.qty,2) END)  as total_sum_with_tax',
                'round(sum({{INVOICE_PAYMENTS}}.amount),2) as total_sum_payment',
            ])
            ->joinWith('invoiceLineItem')
            ->joinWith('invoicePayment')
            ->where(['{{INVOICES}}.id' => $id])->one();

        $total_sum_payment = $invoice->total_sum_payment;
        $total_sum = $invoice->total_sum_with_tax;

        if ($total_sum > 0) {
            $status = 1;
        } else {
            $status = -2;
        }
        if (($total_sum_payment >= $total_sum) && ($total_sum_payment > 0)) {
            $status = 2;
        }
        if ($invoice->status != 3) {
            $invoice->status = $status;
            $invoice->save();
        }
        return;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->inv_date = $this->toAppDate($this->inv_date);
        $this->due_date = $this->toAppDate($this->due_date);

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->inv_date = $this->toStorageDate($this->inv_date);
        $this->due_date = $this->toStorageDate($this->due_date);

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceLineItem()
    {
        return $this->hasMany(InvoiceLineItem::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicePayment()
    {
        return $this->hasMany(InvoicePayment::className(), ['invoice_id' => 'id']);
    }

    /**
     * Returns list of possible terms
     *
     * @param null $id
     * @return array|mixed
     */
    public static function getTerms($id = null)
    {
        $result = [
            0 => '5 Days',
            1 => '10 Days',
            2 => '15 Days',
            3 => '20 Days',
            4 => '30 Days',
            5 => '45 Days',
            98 => 'Due on Receipt',
            99 => 'Custom'
        ];
        if (array_key_exists($id, $result)) {
            return $result[$id];
        } else {
            return $result;
        }
    }

    /**
     * Returns list of possible statuses
     *
     * @param null $id
     * @return array|mixed|string
     */
    public static function getStatus($id = null)
    {
        $result = [
            -2 => 'In Progress',
            1 => 'Unpaid',
            2 => 'Paid',
            3 => 'Voided'
        ];
        if (isset($id)) {
            if (array_key_exists($id, $result)) {
                return $result[$id];
            } else {
                return 'Unknown';
            }
        } else {
            return $result;
        }
    }

    /**
     * Returns last invoice number
     *
     * @return mixed
     */
    public static function getLastNumber()
    {
        $query = self::find()
            ->select([
                'inv_number'
            ])
            ->orderBy(['date' => SORT_DESC]);

        return $query->one();
    }

    /**
     * Returns total sum payment for invoice
     *
     * @param $id
     * @return mixed
     */
    public static function getTotalSumPayment($id)
    {
        $query = self::find()
            ->select([
                'round(sum({{INVOICE_PAYMENTS}}.amount),2) as total_sum_payment',
            ])
            ->joinWith('invoicePayment')
            ->where(['{{INVOICES}}.id' => $id]);
        $result = $query->one();

        return $result['total_sum_payment'];
    }

    /**
     * Returns data for widget info
     *
     * @return array
     */
    public static function getInfoWidgetData()
    {
        $userDB = Yii::$app->user->id;
        $sql = " SELECT " .
            " CASE  WHEN i.days<=30 THEN total_sum-total_payment ELSE 0 END as aging30, " .
            " CASE  WHEN i.days>30 and i.days<=60 THEN total_sum-total_payment ELSE 0 END as aging60, " .
            " CASE  WHEN i.days>60 and i.days<=90 THEN total_sum-total_payment ELSE 0 END as aging90, " .
            " CASE  WHEN i.days>90 THEN total_sum-total_payment ELSE 0 END as aging90plus, " .
            " sum(total_sum-total_payment) as aging_total " .
            " FROM " .
            " ( " .
            "   SELECT " .
            "   i.*, " .
            "   ifnull(sum(CASE  WHEN ili.tax = 'T' THEN  round((ili.price_each * ili.qty*(100+i.tax_rate))/100,2) ELSE round(ili.price_each * ili.qty,2) END),0)  AS total_sum " .
            "   FROM ( " .
            "   SELECT " .
            "     i.id, " .
            "     DATEDIFF(now(),i.inv_date) AS days, " .
            "     i.tax_rate, ifnull(sum(ip.amount),0) total_payment " .
            " FROM " .
            " " . $userDB . ".INVOICES i LEFT JOIN " . $userDB . ".INVOICE_PAYMENTS ip ON i.id=ip.invoice_id " .
            " WHERE i.status=1 " .
            " GROUP BY i.id, i.inv_date,i.tax_rate " .
            " ) i LEFT JOIN " . $userDB . ".INVOICE_LINE_ITEMS ili ON i.id=ili.invoice_id " .
            ") i ";
        $results = Yii::$app->db->createCommand($sql)->queryAll();

        $res = [
            'aging30' => 0,
            'aging60' => 0,
            'aging90' => 0,
            'aging90plus' => 0,
            'aging_total' => 0
        ];
        foreach ($results as $result) {
            $res['aging30'] = $result['aging30'];
            $res['aging60'] = $result['aging60'];
            $res['aging90'] = $result['aging90'];
            $res['aging90plus'] = $result['aging90plus'];
            $res['aging_total'] = $result['aging_total'];
        }

        return $res;
    }
}
