<?php

namespace common\widgets\ModalProcessing;

use yii\bootstrap\Modal;

class ModalProcessing extends \yii\bootstrap\Widget
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $assetPath = '@common/widgets/ModalProcessing/assets';
        \Yii::$app->getAssetManager()
            ->publish($assetPath);
        $publishedUrl = \Yii::$app->getAssetManager()
            ->getPublishedUrl($assetPath);
        Modal::begin([
            'id' => 'modalProcessing',
            'closeButton' => false,
        ]);
        echo <<<HTML
<div style="height:200px;">
    <div class="text-center">
        <img src="$publishedUrl/images/processing_128x128.gif" class="icon"/>
        <h4>Processing...</h4>
    </div>
</div>
HTML;
        Modal::end();
    }
}
