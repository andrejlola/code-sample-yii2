<?php

namespace common\components;

use Yii;
use yii\i18n\Formatter;
use common\models\backend\Category;

/**
 * Global Application Helper
 */
class Helper
{
    /**
     * Returns DB name for given data source name
     *
     * @return string
     */
    public static function getAppDb()
    {
        if (preg_match('/dbname=([A-Za-z]+)/', Category::getDb()->dsn, $matches)) {
            return $matches[1];
        } else {
            return '';
        }
    }

    /**
     * Returns date format
     *
     * @return string
     */
    public function getDateFormat()
    {
        return 'm/d/Y';
    }

    /**
     * Returns time format
     *
     * @return string
     */
    public function getDateTimeFormat()
    {
        return 'm/d/Y H:i:s';
    }

    /**
     * Returns DB date format
     *
     * @return string
     */
    public function getStorageDateFormat()
    {
        return 'Y-m-d';
    }

    /**
     * Returns DB datetime format
     *
     * @return string
     */
    public function getStorageDateTimeFormat()
    {
        return 'Y-m-d H:i:s';
    }

    /**
     * Converts date to storage date format
     *
     * @param $value string Input date
     * @return false|null|string
     */
    public static function toStorageDate($value)
    {
        if ($value = strtotime($value)) {
            return date(self::getStorageDateFormat(), $value);
        }

        return null;
    }

    /**
     * Converts date to application date format
     *
     * @param $value string Input date
     * @return false|string
     */
    public static function toAppDate($value)
    {
        if ($value && $value != '0000-00-00') {
            if ($value = strtotime($value)) {
                return date(self::getDateFormat(), $value);
            }
        }

        return '';
    }

    /**
     * Formats value to numeric value
     *
     * @param $value
     * @param string $sign
     * @return string
     */
    public static function formatNumber($value, $sign = '$')
    {
        if ($value == 0) {
            return '--';
        } else {
            return $sign . ' ' . Yii::$app->formatter->asDecimal($value, 2);
        }
    }

    /**
     * Converts value to integer
     *
     * @param $value
     * @return int
     */
    public static function getIntegerValue($value)
    {
        return isset($value) ? $value : 0;
    }

    /**
     * Returns date with first day of the year for current year
     *
     * @return string
     */
    public static function getCurrentYearDate()
    {
        return date("Y") . '-1-1';
    }

    /**
     * Returns current year
     *
     * @return false|string
     */
    public static function getCurrentYear()
    {
        return date("Y");
    }

    /**
     * Returns next year date
     *
     * @return string
     */
    public static function getNextYearDate()
    {
        return (date("Y") + 1) . '-1-1';
    }
}
