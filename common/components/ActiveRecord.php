<?php

namespace common\components;

use Yii;

/**
 * This is the common class for some models.
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (array_key_exists('id', $this->getAttributes()) && is_null($this->id)) {
                    $this->id = self::generateId();
                }
                if ($this->hasAttribute('date_entered')) {
                    $this->date_entered = (new \DateTime())->format(Yii::$app->helper->getStorageDateTimeFormat());
                }

                return true;
            } else {
            }
        }

        return true;
    }

    /**
     * Generates unique identifier
     *
     * @return string
     */
    public static function generateId()
    {
        $id = MD5(implode('.', array(microtime())));
        while (preg_match('/^\d+e/', $id) || $id == '') {
            $id = MD5(implode('.', array(microtime())));
        }

        return $id;
    }
}
