<?php

namespace common\components;

/**
 * @inheritdoc
 */
class GridView extends \yii\grid\GridView
{
    public $rowSelectable = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->rowSelectable) {
            if (is_array($this->tableOptions) && array_key_exists('class', $this->tableOptions)) {
                if (strpos($this->tableOptions['class'], 'row-selectable') === false) {
                    $this->tableOptions['class'] .= ' row-selectable';
                }
            }
        }
        parent::init();
    }
}
