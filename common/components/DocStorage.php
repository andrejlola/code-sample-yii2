<?php

namespace common\components;

use Yii;
use common\models\frontend\Preference;

/**
 * Contains base logic for Doc storage preferences
 */
class DocStorage extends \common\components\ActiveRecord
{
    const ACTIVE_ACTIVE = 'Y';
    const ACTIVE_INACTIVE = 'N';

    /**
     * Checks availability to store documents for user in storage
     *
     * @return bool
     */
    public static function isEnabled()
    {
        $result = Preference::find()
            ->where([
                'pref_userid' => Yii::$app->user->id,
                'pref_key' => 'doc_storage-active'
            ])->one();
        if ($result) {
            $response = (strtoupper($result->pref_value) == self::ACTIVE_ACTIVE) ? true : false;
            return $response;
        }

        return false;
    }

    /**
     * Sets availability of storing documents for user in storage to enabled
     *
     * @return bool
     */
    public static function Enable()
    {
        self::Disable();
        $newKey = new Preference();
        $newKey->set('doc_storage-active', self::ACTIVE_ACTIVE, Yii::$app->user->id);
        if (!$newKey->save()) {
            return false;
        }

        return true;
    }

    /**
     * Sets availability of storing documents for user in storage to disabled
     *
     * @return bool
     */
    public static function Disable()
    {
        $result = Preference::find()
            ->where([
                'pref_userid' => Yii::$app->user->id,
                'pref_key' => 'doc_storage-active'
            ])->one();
        if (count($result) > 0) {
            $result->delete();
        }
    }
}