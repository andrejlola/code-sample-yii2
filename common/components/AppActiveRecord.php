<?php

namespace common\components;

use Yii;
use app\components\exceptions\AccountReadonlyException;
use yii\db\Connection;

/**
 * Base class for Active Record models
 * @package common\components
 */
class AppActiveRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        /** @var Connection $connection */
        $connection = Yii::$app->app_db;
        $connection->dsn = str_replace('{dbname}', Yii::$app->user->id, $connection->dsn);

        return $connection;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (Yii::$app->user->identity->isAccountReadonlyMode()) {
                throw new AccountReadonlyException();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (Yii::$app->user->identity->isAccountReadonlyMode()) {
            throw new AccountReadonlyException();
        }
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (array_key_exists('id', $this->getAttributes()) && is_null($this->id)) {
                    $this->id = self::generateId();
                }
                if ($this->hasAttribute('date_entered')) {
                    $this->date_entered = (new \DateTime())->format(Yii::$app->helper->getStorageDateTimeFormat());
                }

                return true;
            } else {
            }
        }

        return true;
    }

    /**
     * Converts datetime to storage datetime sting format
     *
     * @param $value
     * @return false|null|string
     */
    public function toStorageDate($value)
    {
        if ($value = strtotime($value)) {
            return date(Yii::$app->helper->getStorageDateFormat(), $value);
        }

        return null;
    }

    /**
     * Convert datetime to application format
     *
     * @param $value
     * @return false|string
     */
    public function toAppDate($value)
    {
        if ($value && $value != '0000-00-00') {
            if ($value = strtotime($value)) {
                return date(Yii::$app->helper->getDateFormat(), $value);
            }
        }

        return '';
    }
}
