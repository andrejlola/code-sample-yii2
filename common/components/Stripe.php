<?php

namespace common\components;

use Stripe\Customer;

/**
 * Facade for working with Stripe payment system
 * @package common\components
 */
class Stripe extends \yii\base\Component
{
    /**
     * Returns client secret key
     *
     * @return string
     */
    protected static function getApiKey()
    {
        return \Yii::$app->params['stripe']['secretKey'];
    }

    /**
     * Set client secret key to Stripe client instance
     */
    protected static function ApplyApiKey()
    {
        \Stripe\Stripe::setApiKey(self::getApiKey());
    }

    /**
     * Creates customer with subscription in Stripe system
     *
     * @param $stripeToken
     * @param $subscriptionId
     * @param $billingEmail
     * @param $billingFirstname
     * @param $billingLastname
     * @return mixed
     */
    public static function createCustomer(
        $stripeToken,
        $subscriptionId,
        $billingEmail,
        $billingFirstname,
        $billingLastname
    ) {
        self::ApplyApiKey();
        $customer = Customer::create([
            'source' => $stripeToken,
            'plan' => $subscriptionId,
            'email' => $billingEmail,
            'description' => $billingFirstname . ' ' . $billingLastname
        ]);

        return $customer;
    }

    /**
     * Retrieve Stripe customer
     *
     * @param $id
     * @return mixed
     */
    public static function getCustomer($id)
    {
        self::ApplyApiKey();

        return Customer::retrieve($id);
    }

    /**
     * Deletes payment card for Stripe customer
     *
     * @param $customerId
     * @return bool
     */
    public static function DeleteCard($customerId)
    {
        self::ApplyApiKey();
        $result = false;
        $customer = self::getCustomer($customerId);
        foreach ($customer->sources->data as $card) {
            $result = $card->delete();
        }

        return $result;
    }

    /**
     * Adds payment card to Stripe customer
     *
     * @param $customerId
     * @param $cardId
     * @return mixed
     */
    public static function AddCard($customerId, $cardId)
    {
        self::ApplyApiKey();
        $customer = self::getCustomer($customerId);

        return $customer->sources->create(['card' => $cardId]);
    }

    /**
     * Checks has customer a subscription or nor
     *
     * @param $customerId
     * @return bool
     */
    public static function HasSubscription($customerId)
    {
        self::ApplyApiKey();
        $customer = self::getCustomer($customerId);

        return count($customer->subscriptions->all()) > 0;
    }

    /**
     * Cancels customer subscriptions
     *
     * @param $customerId
     * @return mixed
     */
    public static function CancelSubscription($customerId)
    {
        self::ApplyApiKey();
        $customer = self::getCustomer($customerId);

        return $customer->cancelSubscription();
    }
}
